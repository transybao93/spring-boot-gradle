package transybao.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PutMapping;
import java.util.Map;
import java.util.Collections;

@SpringBootApplication
@RestController
public class SpringBootDemo {

	@GetMapping("/h")
    public Map home() {
        // return "Hello Docker World";
        return Collections.singletonMap("response", "Hello World");
    }

    @RequestMapping("/hello3")
    public String hello() {
        return "Xin chào các bạn";
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringBootDemo.class, args);
	}

}
